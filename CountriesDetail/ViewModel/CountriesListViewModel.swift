//
//  CountriesListViewModel.swift
//  CountriesDetail
//
//  Created by Balamurugan Gopal on 3/2/18.
//  Copyright © 2018 Balamurugan Gopal. All rights reserved.
//

import Foundation

// This protocol helps to delegate any UI changes to ViewController
protocol CountriesListViewDelegate: class {
    
    func updateUI()
    func showError(_ error: ApiError)
    
}

class CountriesListViewModel {
    
    let countryRestAPI = CountriesRestApi()
    weak var delegate: CountriesListViewDelegate?
    private var countries: [Country]?
    private var filteredCountries: [Country]?
    
    func fetchCountries() {
        countryRestAPI.fetchCountries { [weak self] (countries, error) in
            guard error == nil else {
                self?.delegate?.showError(error!)
                return
            }
            guard let countries = countries, let strongSelf = self else {
                self?.delegate?.showError(.dataIsNil)
                return
            }
            // strongSelf helps to avoid self object deallocation between updating the model and updating UI
            strongSelf.setCountries(countries)
            strongSelf.delegate?.updateUI()
        }
    }
    
    // This method sets the private countries object and also helps in automating the unit testing for this viewModel
    func setCountries(_ countries: [Country]) {
        self.countries = countries
        // Initially filteredCountries object will contain all countries
        filteredCountries = countries
    }
    
    func numberofItemsInSection(section: Int) -> Int {
        return filteredCountries?.count ?? 0
    }
    
    func titleForItemAt(row: Int) -> String {
        return filteredCountries?[row].name ?? ""
    }
    
    func getCountryAt(row: Int) -> Country? {
        return filteredCountries?[row]
    }
    
    func filterCountriesBy(name: String) {
        if name.isEmpty {
            filteredCountries = countries
        } else {
            filteredCountries = countries?.filter {
                return $0.name?.lowercased().contains(name.lowercased()) ?? false
            }
        }
    }
    
}
