//
//  CountryDetailViewModel.swift
//  CountriesDetail
//
//  Created by Balamurugan Gopal on 3/2/18.
//  Copyright © 2018 Balamurugan Gopal. All rights reserved.
//

import UIKit

// This protocol helps to delegate any UI changes to ViewController
protocol CountryDetailViewDelegage: class {
    
    func updateCountryFlag(image: UIImage)
    func showError(_ error: ApiError)
    
}

class CountryDetailViewModel {
    
    let countriesRestApi = CountriesRestApi()
    private let country: Country
    weak var delegate: CountryDetailViewDelegage?
    
    init(country: Country) {
        self.country = country
    }
    
    func fetchCountryFlag() {
        guard let countryCode = country.alpha2Code else {
            delegate?.showError(.dataIsNil)
            return
        }
        // countryCode(alpha2Code) needs to passed as lower case because image file names are in lower case
        countriesRestApi.fetchCountryFlag(countryCode: countryCode.lowercased()) { [weak self] (data, error) in
            guard error == nil else {
                self?.delegate?.showError(error!)
                return
            }
            guard let data = data else {
                self?.delegate?.showError(.dataIsNil)
                return
            }
            if let image = UIImage(data: data) {
                self?.delegate?.updateCountryFlag(image: image)
            } else {
                self?.delegate?.showError(.unknown)
            }
        }
    }
    
    func getCountryName() -> String {
        return country.name ?? ""
    }
    
    func getCapital() -> String {
        return country.capital ?? ""
    }
    
    func getRegion() -> String {
        return country.region ?? ""
    }
    
    func getSubRegion() -> String {
        return country.subregion ?? ""
    }
    
    func getAlpha2Code() -> String {
        return country.alpha2Code ?? ""
    }
    
    func getAlpha3Code() -> String {
        return country.alpha3Code ?? ""
    }
    
    func getPopulation() -> String {
        return country.population != nil ? "\(country.population!)" : ""
    }
    
    func getCoordinates() -> (latitude: Double?, longitude: Double?) {
        // Ensure 2 values (latitude/nil and longidtude/nil) are present, to avoid crash with array out of bound index
        if country.latlng?.count == 2 {
            return (country.latlng?[0], country.latlng?[1])
        } else {
            return (nil, nil)
        }
    }
    
    func getTimezones() -> String {
        let timezones = country.timezones?.joined(separator: ", ")
        return timezones ?? ""
    }
    
    func getCallingCodes() -> String {
        let callingCodes = country.callingCodes?.joined(separator: ", ")
        return callingCodes ?? ""
    }
    
    func getBorders() -> String {
        let borders = country.borders?.joined(separator: ", ")
        return borders ?? ""
    }
    
    func getCurrencies() -> String {
        let currencies = country.currencies?.joined(separator: ", ")
        return currencies ?? ""
    }
    
    func getLanguages() -> String {
        let languages = country.languages?.joined(separator: ", ")
        return languages ?? ""
    }
    
    func getArea() -> Double? {
        return country.area
    }
    
}
