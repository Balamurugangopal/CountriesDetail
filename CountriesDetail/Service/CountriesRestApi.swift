//
//  CountriesRestApi.swift
//  CountriesDetail
//
//  Created by Balamurugan Gopal on 3/2/18.
//  Copyright © 2018 Balamurugan Gopal. All rights reserved.
//

import Foundation

struct Constants {
    
    static let mashapeKey = "dVyN6JJaQOmsh2rwBqD7YPDza9o8p1R5qBJjsnYqxVsq66hkdH"
    
}

struct UrlStrings {
    
    static let countries = "https://restcountries-v1.p.mashape.com/all"
    static let countryFlag = "http://www.geonames.org/flags/l/"
    
}

typealias CountryResult = ([Country]?, ApiError?) -> Void

// This class hanldes Api calls specific to Countries Rest Apis
class CountriesRestApi {
    
    private let coreApi = CoreApi()
    
    func fetchCountries(completion: @escaping CountryResult) {
        guard let url = URL(string: UrlStrings.countries) else {
            completion(nil, .wrongUrl)
            return
        }
        // form the header with mashapekey and json
        let requestHeader = ["X-Mashape-key": Constants.mashapeKey, "Accept": "application/json"]
        coreApi.sendRequest(url: url, htttType: .get, header: requestHeader, body: nil) { (data, error) in
            if let data = data {
                do {
                    let countries = try JSONDecoder().decode([Country].self, from: data)
                    completion(countries, nil)
                } catch {
                    completion(nil, .jsonDecodeFailed)
                }
            } else {
                completion(nil, error)
            }
        }
    }
    
    func fetchCountryFlag(countryCode: String, completion: @escaping ApiResult) {
        // Append countryCode and .gif to countryFlagUrlString
        let countryFlagUrlString = UrlStrings.countryFlag + "\(countryCode).gif"
        guard let url = URL(string: countryFlagUrlString) else {
            completion(nil, .wrongUrl)
            return
        }
        coreApi.sendRequest(url: url, htttType: .get, header: nil, body: nil) { (data, error) in
            if let data = data {
                completion(data, nil)
            } else {
                completion(nil, error)
            }
        }
    }
    
}
