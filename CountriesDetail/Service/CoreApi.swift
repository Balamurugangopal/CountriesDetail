//
//  CoreApi.swift
//  CountriesDetail
//
//  Created by Balamurugan Gopal on 3/2/18.
//  Copyright © 2018 Balamurugan Gopal. All rights reserved.
//

import Foundation

enum HttpType: String {
    
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    
}

enum ApiError: Error {
    
    case badStatus
    case dataIsNil
    case jsonDecodeFailed
    case wrongUrl
    case unknown
    
}

typealias ApiResult = (Data?, ApiError?) -> Void

// This class handles network communications
class CoreApi {
    
    func sendRequest(url: URL, htttType: HttpType, header: [String: String]?, body: Data? ,completion: @escaping ApiResult) {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = htttType.rawValue
        urlRequest.allHTTPHeaderFields = header
        urlRequest.httpBody = body
        let dataTask =  URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(nil, .badStatus)
                return
            }
            if httpResponse.statusCode != 200 {
                completion(nil, .badStatus)
            } else if error == nil {
                completion(data, nil)
            } else {
                completion(nil, .dataIsNil)
            }
        }
        dataTask.resume()
    }
    
}
