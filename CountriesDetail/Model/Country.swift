//
//  Countries.swift
//  CountriesDetail
//
//  Created by Balamurugan Gopal on 3/2/18.
//  Copyright © 2018 Balamurugan Gopal. All rights reserved.
//

import Foundation

struct Country: Decodable {
    
    let name: String?
    let alpha2Code: String?
    let alpha3Code: String?
    let callingCodes: [String]?
    let capital: String?
    let region: String?
    let subregion: String?
    let population: Int?
    let latlng: [Double]?
    let area: Double?
    let timezones: [String]?
    let borders: [String]?
    let currencies: [String]?
    let languages: [String]?
    
}
