//
//  CountryDetailViewController.swift
//  CountriesDetail
//
//  Created by Balamurugan Gopal on 3/2/18.
//  Copyright © 2018 Balamurugan Gopal. All rights reserved.
//

import UIKit
import MapKit

class CountryDetailViewController: UIViewController {
    
    private var viewModel: CountryDetailViewModel?
    
    let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        // this enables autolayout for scrollView
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    let contentView: UIView = {
        let contentView = UIView()
        contentView.translatesAutoresizingMaskIntoConstraints = false
        return contentView
    }()
    let flagImageView: UIImageView = {
        let flagImageView = UIImageView()
        flagImageView.translatesAutoresizingMaskIntoConstraints = false
        return flagImageView
    }()
    let mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.translatesAutoresizingMaskIntoConstraints = false
        return mapView
    }()
    let capitalTitleLabel: UILabel = {
        let capitalTitleLabel = UILabel()
        capitalTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        capitalTitleLabel.text = NSLocalizedString("Capital  :", comment: "Capital  :")
        // Though its static text with less characters, support multiple lines for any future localization support
        capitalTitleLabel.numberOfLines = 0
        capitalTitleLabel.textAlignment = .right
        return capitalTitleLabel
    }()
    let capitalValueLabel: UILabel = {
        let capitalValueLabel = UILabel()
        capitalValueLabel.translatesAutoresizingMaskIntoConstraints = false
        capitalValueLabel.numberOfLines = 0
        return capitalValueLabel
    }()
    let regionTitleLabel: UILabel = {
        let regionTitleLabel = UILabel()
        regionTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        regionTitleLabel.text = NSLocalizedString("Region  :", comment: "Region  :")
        // Though its static text with less characters, support multiple lines for future any localization support
        regionTitleLabel.numberOfLines = 0
        regionTitleLabel.textAlignment = .right
        return regionTitleLabel
    }()
    let regionValueLabel: UILabel = {
        let regionValueLabel = UILabel()
        regionValueLabel.translatesAutoresizingMaskIntoConstraints = false
        regionValueLabel.numberOfLines = 0
        return regionValueLabel
    }()
    let subRegionTitleLabel: UILabel = {
        let subRegionTitleLabel = UILabel()
        subRegionTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        subRegionTitleLabel.text = NSLocalizedString("Sub Region  :", comment: "Sub Region  :")
        subRegionTitleLabel.numberOfLines = 0
        subRegionTitleLabel.textAlignment = .right
        return subRegionTitleLabel
    }()
    let subRegionValueLabel: UILabel = {
        let subRegionValueLabel = UILabel()
        subRegionValueLabel.translatesAutoresizingMaskIntoConstraints = false
        subRegionValueLabel.numberOfLines = 0
        return subRegionValueLabel
    }()
    let countryCodeTitleLabel: UILabel = {
        let countryCodeTitleLabel = UILabel()
        countryCodeTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        countryCodeTitleLabel.text = NSLocalizedString("Country Code  :", comment: "Country Code  :")
        countryCodeTitleLabel.numberOfLines = 0
        countryCodeTitleLabel.textAlignment = .right
        return countryCodeTitleLabel
    }()
    let countryCodeValueLabel: UILabel = {
        let countryCodeValueLabel = UILabel()
        countryCodeValueLabel.translatesAutoresizingMaskIntoConstraints = false
        return countryCodeValueLabel
    }()
    let populationTitleLabel: UILabel = {
        let populationTitleLabel = UILabel()
        populationTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        populationTitleLabel.text = NSLocalizedString("Population  :", comment: "Population  :")
        populationTitleLabel.numberOfLines = 0
        populationTitleLabel.textAlignment = .right
        return populationTitleLabel
    }()
    let populationValueLabel: UILabel = {
        let populationValueLabel = UILabel()
        populationValueLabel.translatesAutoresizingMaskIntoConstraints = false
        populationValueLabel.numberOfLines = 0
        return populationValueLabel
    }()
    let timezonesTitleLabel: UILabel = {
        let timezonesTitleLabel = UILabel()
        timezonesTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        timezonesTitleLabel.text = NSLocalizedString("Timezones  :", comment: "Timezones  :")
        timezonesTitleLabel.numberOfLines = 0
        timezonesTitleLabel.textAlignment = .right
        return timezonesTitleLabel
    }()
    let timezonesValueLabel: UILabel = {
        let timezonesValueLabel = UILabel()
        timezonesValueLabel.translatesAutoresizingMaskIntoConstraints = false
        timezonesValueLabel.numberOfLines = 0
        return timezonesValueLabel
    }()
    let callingCodesTitleLabel: UILabel = {
        let callingCodesTitleLabel = UILabel()
        callingCodesTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        callingCodesTitleLabel.text = NSLocalizedString("Calling Codes  :", comment: "Calling Codes  :")
        callingCodesTitleLabel.numberOfLines = 0
        callingCodesTitleLabel.textAlignment = .right
        return callingCodesTitleLabel
    }()
    let callingCodesValueLabel: UILabel = {
        let callingCodesValueLabel = UILabel()
        callingCodesValueLabel.translatesAutoresizingMaskIntoConstraints = false
        callingCodesValueLabel.numberOfLines = 0
        return callingCodesValueLabel
    }()
    let currenciesTitleLabel: UILabel = {
        let currenciesTitleLabel = UILabel()
        currenciesTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        currenciesTitleLabel.text = NSLocalizedString("Currencies  :", comment: "Currencies  :")
        currenciesTitleLabel.numberOfLines = 0
        currenciesTitleLabel.textAlignment = .right
        return currenciesTitleLabel
    }()
    let bordersValueLabel: UILabel = {
        let bordersValueLabel = UILabel()
        bordersValueLabel.translatesAutoresizingMaskIntoConstraints = false
        bordersValueLabel.numberOfLines = 0
        return bordersValueLabel
    }()
    let bordersTitleLabel: UILabel = {
        let bordersTitleLabel = UILabel()
        bordersTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        bordersTitleLabel.text = NSLocalizedString("Borders  :", comment: "Borders  :")
        bordersTitleLabel.numberOfLines = 0
        bordersTitleLabel.textAlignment = .right
        return bordersTitleLabel
    }()
    let currenciesValueLabel: UILabel = {
        let currenciesValueLabel = UILabel()
        currenciesValueLabel.translatesAutoresizingMaskIntoConstraints = false
        currenciesValueLabel.numberOfLines = 0
        return currenciesValueLabel
    }()
    let languagesTitleLabel: UILabel = {
        let languagesTitleLabel = UILabel()
        languagesTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        languagesTitleLabel.text = NSLocalizedString("Languages  :", comment: "Languages  :")
        languagesTitleLabel.numberOfLines = 0
        languagesTitleLabel.textAlignment = .right
        return languagesTitleLabel
    }()
    let languagesValueLabel: UILabel = {
        let languagesValueLabel = UILabel()
        languagesValueLabel.translatesAutoresizingMaskIntoConstraints = false
        languagesValueLabel.numberOfLines = 0
        return languagesValueLabel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel?.delegate = self
        title = viewModel?.getCountryName()
        viewModel?.fetchCountryFlag()
        addSubviews()
        setupLayout()
        assignValuesToLabels()
        annotateMapView()
    }
    
    func initViewModel(country: Country) {
        viewModel = CountryDetailViewModel(country: country)
    }
    
    private func addSubviews() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(flagImageView)
        contentView.addSubview(mapView)
        contentView.addSubview(capitalTitleLabel)
        contentView.addSubview(capitalValueLabel)
        contentView.addSubview(regionTitleLabel)
        contentView.addSubview(regionValueLabel)
        contentView.addSubview(subRegionTitleLabel)
        contentView.addSubview(subRegionValueLabel)
        contentView.addSubview(countryCodeTitleLabel)
        contentView.addSubview(countryCodeValueLabel)
        contentView.addSubview(populationTitleLabel)
        contentView.addSubview(populationValueLabel)
        contentView.addSubview(timezonesTitleLabel)
        contentView.addSubview(timezonesValueLabel)
        contentView.addSubview(callingCodesTitleLabel)
        contentView.addSubview(callingCodesValueLabel)
        contentView.addSubview(bordersTitleLabel)
        contentView.addSubview(bordersValueLabel)
        contentView.addSubview(currenciesTitleLabel)
        contentView.addSubview(currenciesValueLabel)
        contentView.addSubview(languagesTitleLabel)
        contentView.addSubview(languagesValueLabel)
    }
    
    private func setupLayout() {
        let topDefaultConstant: CGFloat = 10
        let leftDefaultConstant: CGFloat = 10
        let rightDefaultConstant: CGFloat = -10
        let bottomDefaultConstant: CGFloat = -10
        
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        contentView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        contentView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        contentView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        flagImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: topDefaultConstant).isActive = true
        flagImageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        flagImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        flagImageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        mapView.topAnchor.constraint(equalTo: flagImageView.bottomAnchor, constant: topDefaultConstant).isActive = true
        mapView.heightAnchor.constraint(equalToConstant: 250).isActive = true
        mapView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: leftDefaultConstant).isActive = true
        mapView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: rightDefaultConstant).isActive = true
        
        capitalValueLabel.topAnchor.constraint(equalTo: mapView.bottomAnchor, constant: topDefaultConstant).isActive = true
        capitalValueLabel.leftAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        capitalValueLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: rightDefaultConstant).isActive = true
        
        capitalTitleLabel.topAnchor.constraint(equalTo: capitalValueLabel.topAnchor).isActive = true
        capitalTitleLabel.bottomAnchor.constraint(equalTo: capitalValueLabel.bottomAnchor).isActive = true
        capitalTitleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: leftDefaultConstant).isActive = true
        capitalTitleLabel.rightAnchor.constraint(equalTo: capitalValueLabel.leftAnchor, constant: rightDefaultConstant).isActive = true
        
        regionValueLabel.topAnchor.constraint(equalTo: capitalValueLabel.bottomAnchor, constant: topDefaultConstant).isActive = true
        regionValueLabel.leftAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        regionValueLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: rightDefaultConstant).isActive = true
        
        regionTitleLabel.topAnchor.constraint(equalTo: regionValueLabel.topAnchor).isActive = true
        regionTitleLabel.bottomAnchor.constraint(equalTo: regionValueLabel.bottomAnchor).isActive = true
        regionTitleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: topDefaultConstant).isActive = true
        regionTitleLabel.rightAnchor.constraint(equalTo: regionValueLabel.leftAnchor, constant: rightDefaultConstant).isActive = true
        
        subRegionValueLabel.topAnchor.constraint(equalTo: regionValueLabel.bottomAnchor, constant: topDefaultConstant).isActive = true
        subRegionValueLabel.leftAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        subRegionValueLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: rightDefaultConstant).isActive = true
        
        subRegionTitleLabel.topAnchor.constraint(equalTo: subRegionValueLabel.topAnchor).isActive = true
        subRegionTitleLabel.bottomAnchor.constraint(equalTo: subRegionValueLabel.bottomAnchor).isActive = true
        subRegionTitleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: leftDefaultConstant).isActive = true
        subRegionTitleLabel.rightAnchor.constraint(equalTo: subRegionValueLabel.leftAnchor, constant: rightDefaultConstant).isActive = true
        
        countryCodeValueLabel.topAnchor.constraint(equalTo: subRegionValueLabel.bottomAnchor, constant: topDefaultConstant).isActive = true
        countryCodeValueLabel.leftAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        countryCodeValueLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: rightDefaultConstant).isActive = true
        
        countryCodeTitleLabel.topAnchor.constraint(equalTo: countryCodeValueLabel.topAnchor).isActive = true
        countryCodeTitleLabel.bottomAnchor.constraint(equalTo: countryCodeValueLabel.bottomAnchor).isActive = true
        countryCodeTitleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: leftDefaultConstant).isActive = true
        countryCodeTitleLabel.rightAnchor.constraint(equalTo: countryCodeValueLabel.leftAnchor, constant: rightDefaultConstant).isActive = true
        
        populationValueLabel.topAnchor.constraint(equalTo: countryCodeValueLabel.bottomAnchor, constant: topDefaultConstant).isActive = true
        populationValueLabel.leftAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        populationValueLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: rightDefaultConstant).isActive = true
        
        populationTitleLabel.topAnchor.constraint(equalTo: populationValueLabel.topAnchor).isActive = true
        populationTitleLabel.bottomAnchor.constraint(equalTo: populationValueLabel.bottomAnchor).isActive = true
        populationTitleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: leftDefaultConstant).isActive = true
        populationTitleLabel.rightAnchor.constraint(equalTo: populationValueLabel.leftAnchor, constant: rightDefaultConstant).isActive = true
        
        timezonesValueLabel.topAnchor.constraint(equalTo: populationValueLabel.bottomAnchor, constant: topDefaultConstant).isActive = true
        timezonesValueLabel.leftAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        timezonesValueLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: rightDefaultConstant).isActive = true
        
        timezonesTitleLabel.topAnchor.constraint(equalTo: timezonesValueLabel.topAnchor).isActive = true
        timezonesTitleLabel.bottomAnchor.constraint(equalTo: timezonesValueLabel.bottomAnchor).isActive = true
        timezonesTitleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: leftDefaultConstant).isActive = true
        timezonesTitleLabel.rightAnchor.constraint(equalTo: timezonesValueLabel.leftAnchor, constant: rightDefaultConstant).isActive = true
        
        callingCodesValueLabel.topAnchor.constraint(equalTo: timezonesValueLabel.bottomAnchor, constant: topDefaultConstant).isActive = true
        callingCodesValueLabel.leftAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        callingCodesValueLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: rightDefaultConstant).isActive = true
        
        callingCodesTitleLabel.topAnchor.constraint(equalTo: callingCodesValueLabel.topAnchor).isActive = true
        callingCodesTitleLabel.bottomAnchor.constraint(equalTo: callingCodesValueLabel.bottomAnchor).isActive = true
        callingCodesTitleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: leftDefaultConstant).isActive = true
        callingCodesTitleLabel.rightAnchor.constraint(equalTo: callingCodesValueLabel.leftAnchor, constant: rightDefaultConstant).isActive = true
        
        bordersValueLabel.topAnchor.constraint(equalTo: callingCodesValueLabel.bottomAnchor, constant: topDefaultConstant).isActive = true
        bordersValueLabel.leftAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        bordersValueLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: rightDefaultConstant).isActive = true
        
        bordersTitleLabel.topAnchor.constraint(equalTo: bordersValueLabel.topAnchor).isActive = true
        bordersTitleLabel.bottomAnchor.constraint(equalTo: bordersValueLabel.bottomAnchor).isActive = true
        bordersTitleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: leftDefaultConstant).isActive = true
        bordersTitleLabel.rightAnchor.constraint(equalTo: bordersValueLabel.leftAnchor, constant: rightDefaultConstant).isActive = true
        
        currenciesValueLabel.topAnchor.constraint(equalTo: bordersValueLabel.bottomAnchor, constant: topDefaultConstant).isActive = true
        currenciesValueLabel.leftAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        currenciesValueLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: rightDefaultConstant).isActive = true
        
        currenciesTitleLabel.topAnchor.constraint(equalTo: currenciesValueLabel.topAnchor).isActive = true
        currenciesTitleLabel.bottomAnchor.constraint(equalTo: currenciesValueLabel.bottomAnchor).isActive = true
        currenciesTitleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: leftDefaultConstant).isActive = true
        currenciesTitleLabel.rightAnchor.constraint(equalTo: currenciesValueLabel.leftAnchor, constant: rightDefaultConstant).isActive = true
        
        languagesValueLabel.topAnchor.constraint(equalTo: currenciesValueLabel.bottomAnchor, constant: topDefaultConstant).isActive = true
        // Pin the bottom anchor to contentView bottomAnchor to enable scroll
        languagesValueLabel.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: bottomDefaultConstant).isActive = true
        languagesValueLabel.leftAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        languagesValueLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: rightDefaultConstant).isActive = true
        
        languagesTitleLabel.topAnchor.constraint(equalTo: languagesValueLabel.topAnchor).isActive = true
        languagesTitleLabel.bottomAnchor.constraint(equalTo: languagesValueLabel.bottomAnchor).isActive = true
        languagesTitleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: leftDefaultConstant).isActive = true
        languagesTitleLabel.rightAnchor.constraint(equalTo: languagesValueLabel.leftAnchor, constant: rightDefaultConstant).isActive = true
    }
    
    private func assignValuesToLabels() {
        capitalValueLabel.text = viewModel?.getCapital()
        regionValueLabel.text = viewModel?.getRegion()
        subRegionValueLabel.text = viewModel?.getSubRegion()
        countryCodeValueLabel.text = viewModel?.getAlpha3Code()
        populationValueLabel.text = viewModel?.getPopulation()
        timezonesValueLabel.text = viewModel?.getTimezones()
        callingCodesValueLabel.text = viewModel?.getCallingCodes()
        bordersValueLabel.text = viewModel?.getBorders()
        currenciesValueLabel.text = viewModel?.getCurrencies()
        languagesValueLabel.text = viewModel?.getLanguages()
    }
    
    private func annotateMapView() {
    
        let coordinate = viewModel?.getCoordinates()
        guard let lattitude = coordinate?.latitude, let longitude = coordinate?.longitude else {
            showError(.dataIsNil)
            return
        }
        let location = CLLocationCoordinate2D(latitude: lattitude, longitude: longitude)
        var region: MKCoordinateRegion
        
        if let area = viewModel?.getArea(), area > 10000 {
            //used circle radius formulae and adjusted it approximately for country shape 
            let locationDistance = sqrt(area/3.14 * 3000000)
            region = MKCoordinateRegionMakeWithDistance(location, locationDistance, locationDistance)
        } else {
            //if area is nil / smaller countries
            region = MKCoordinateRegionMake(location, MKCoordinateSpanMake(1, 1))
        }
        mapView.setRegion(region, animated: true)
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = viewModel?.getCountryName()
        mapView.addAnnotation(annotation)
    }
    
}

extension CountryDetailViewController: CountryDetailViewDelegage {
    
    func updateCountryFlag(image: UIImage) {
        DispatchQueue.main.sync { [weak self] in
            self?.flagImageView.image = image
        }
    }
    
    func showError(_ error: ApiError) {
        var errorMessage: String
        switch error {
        case .badStatus:
            errorMessage = NSLocalizedString("Country flag download failure. Please check your network connection and try again!", comment: "")
        case .dataIsNil:
            errorMessage = NSLocalizedString("Coordinates are not available for the selected Country", comment: "")
        default:
            errorMessage = NSLocalizedString("Country flag download failure. Please try again later!", comment: "")
        }
        let alertController = UIAlertController(title: NSLocalizedString("Error", comment: "Error"), message: errorMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: NSLocalizedString("Ok", comment: "Ok"), style: .cancel, handler: nil)
        alertController.addAction(okAction)
        
        if Thread.isMainThread {
            self.present(alertController, animated: true, completion: nil)
        } else {
            DispatchQueue.main.sync {[weak self] in
                self?.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
}
