//
//  CountriesListViewController.swift
//  CountriesDetail
//
//  Created by Balamurugan Gopal on 3/2/18.
//  Copyright © 2018 Balamurugan Gopal. All rights reserved.
//

import UIKit

class CountriesListViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var acivityIndicator: UIActivityIndicatorView!
    private let viewModel = CountriesListViewModel()
    let searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.hidesNavigationBarDuringPresentation = true
        searchController.dimsBackgroundDuringPresentation = false
        return searchController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        viewModel.fetchCountries()
        searchController.searchResultsUpdater = self
        self.definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "showCountryDetail" {
            // Don't perform segue if country object is nil
            guard let indexPath = tableView.indexPathForSelectedRow, let _ = viewModel.getCountryAt(row: indexPath.row) else {
                showError(.dataIsNil)
                return false
            }
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCountryDetail" {
            // Dependency Injection: country object
            if let countryDetailViewController = segue.destination as? CountryDetailViewController, let indexPath = tableView.indexPathForSelectedRow, let country = viewModel.getCountryAt(row: indexPath.row) {
                countryDetailViewController.initViewModel(country: country)
            } else {
                showError(.unknown)
            }
        }
    }
    
}

extension CountriesListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberofItemsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "countryNameCell") {
            cell.textLabel?.text = viewModel.titleForItemAt(row: indexPath.row)
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension CountriesListViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text else {
            return
        }
        viewModel.filterCountriesBy(name: searchText)
        tableView.reloadData()
    }
    
}

extension CountriesListViewController: CountriesListViewDelegate {
    
    func updateUI() {
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
            self?.acivityIndicator.stopAnimating()
        }
    }
    
    func showError(_ error: ApiError) {
        var errorMessage: String
        switch error {
        case .badStatus:
            errorMessage = NSLocalizedString("Country rest service failure. Please check your network connection and try again!", comment: "")
        case .dataIsNil, .jsonDecodeFailed, .wrongUrl:
            errorMessage = NSLocalizedString("Country rest service failure. Please try again later!", comment: "")
        case .unknown:
            errorMessage = NSLocalizedString("Something went wrong. Please try again later!", comment: "")
        }
        let alertController = UIAlertController(title: NSLocalizedString("Error", comment: "Error"), message: errorMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: NSLocalizedString("Ok", comment: "Ok"), style: .cancel, handler: nil)
        alertController.addAction(okAction)
        
        if Thread.isMainThread {
            self.present(alertController, animated: true, completion: nil)
            self.acivityIndicator.stopAnimating()
        } else {
            DispatchQueue.main.sync { [weak self] in
                self?.present(alertController, animated: true, completion: nil)
                self?.acivityIndicator.stopAnimating()
            }
        }
    }
    
}
