//
//  CountryDetailViewModelTests.swift
//  CountriesDetailTests
//
//  Created by Balamurugan Gopal on 3/2/18.
//  Copyright © 2018 Balamurugan Gopal. All rights reserved.
//

import XCTest
@testable import CountriesDetail

class CountryDetailViewModelTests: XCTestCase {
    
    var viewModel: CountryDetailViewModel?
    
    override func setUp() {
        super.setUp()
        test_fetchCountry()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_fetchCountry() {
        let countriesListViewModel = CountriesListViewModel()
        let expectation = XCTestExpectation(description: "PerformFetchRequest")
        countriesListViewModel.countryRestAPI.fetchCountries {[weak self] (countries, error) in
            XCTAssertNil(error)
            XCTAssertNotNil(countries)
            countriesListViewModel.setCountries(countries!)
            // Unit testing performed using first Country which is Afghanistan and tests may fail if countries are not recieved in sorted order
            if let country = countriesListViewModel.getCountryAt(row: 0) {
                self?.viewModel = CountryDetailViewModel(country: country)
            }
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 2)
    }
    
    func test_fetchCountryFlag()
    {
        guard let countryCode = viewModel?.getAlpha2Code() else {
            XCTAssertTrue(false)
            return
        }
        let expectation = XCTestExpectation(description: "PerformFetchRequest")
        viewModel?.countriesRestApi.fetchCountryFlag(countryCode: countryCode.lowercased()) { (data, error) in
            XCTAssertNil(error)
            XCTAssertNotNil(data)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 2)
    }
    
    func test_getCountryName() {
        XCTAssertEqual(viewModel?.getCountryName(), "Afghanistan")
    }
    
    func test_getCapital() {
        XCTAssertEqual(viewModel?.getCapital(), "Kabul")
    }
    
    func test_getRegion() {
        XCTAssertEqual(viewModel?.getRegion(), "Asia")
    }
    
    func test_getSubRegion() {
        XCTAssertEqual(viewModel?.getSubRegion(), "Southern Asia")
    }
    
    func test_getAlpha2Code() {
        XCTAssertEqual(viewModel?.getAlpha2Code(), "AF")
    }
    
    func test_getAlpha3Code() {
        XCTAssertEqual(viewModel?.getAlpha3Code(), "AFG")
    }
    
    func test_getCoordinates() {
        let coordinates = viewModel?.getCoordinates()
        XCTAssertEqual(coordinates?.0, 33)
        XCTAssertEqual(coordinates?.1, 65)
    }
    
    func test_getTimezones() {
        XCTAssertEqual(viewModel?.getTimezones(), "UTC+04:30")
    }
    
    func test_getCallingCodes() {
        XCTAssertEqual(viewModel?.getCallingCodes(), "93")
    }
    
    func test_getBorders() {
        XCTAssertEqual(viewModel?.getBorders(), "IRN, PAK, TKM, UZB, TJK, CHN")
    }
    
    func test_getCurrencies() {
        XCTAssertEqual(viewModel?.getCurrencies(), "AFN")
    }
    
    func test_getLanguages() {
        XCTAssertEqual(viewModel?.getLanguages(), "ps, uz, tk")
    }
    
    func test_getArea() {
        XCTAssertEqual(viewModel?.getArea(), 652230.0)
    }
    
}
