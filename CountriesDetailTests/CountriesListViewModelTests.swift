//
//  CountriesListViewModelTests.swift
//  CountriesDetailTests
//
//  Created by Balamurugan Gopal on 3/2/18.
//  Copyright © 2018 Balamurugan Gopal. All rights reserved.
//

import XCTest
@testable import CountriesDetail

class CountriesListViewModelTests: XCTestCase {
    
    let viewModel: CountriesListViewModel = CountriesListViewModel()
    
    override func setUp() {
        super.setUp()
        test_fetchCountries()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_fetchCountries() {
        let expectation = XCTestExpectation(description: "PerformFetchRequest")
        viewModel.countryRestAPI.fetchCountries {[weak self] (countries, error) in
            XCTAssertNil(error)
            XCTAssertNotNil(countries)
            self?.viewModel.setCountries(countries!)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 2)
    }
    
    func test_numberofItemsInSection() {
        XCTAssertEqual(viewModel.numberofItemsInSection(section: 0), 250)
    }
    
    func test_titleForItemAt() {
        XCTAssertEqual(viewModel.titleForItemAt(row: 0), "Afghanistan")
    }
    
    func test_getCountryAt() {
        XCTAssertNotNil(viewModel.getCountryAt(row: 0))
    }
    
    func test_filterCountriesBy() {
        viewModel.filterCountriesBy(name: "united")
        XCTAssertEqual(viewModel.numberofItemsInSection(section: 0), 4)
    }
    
}
